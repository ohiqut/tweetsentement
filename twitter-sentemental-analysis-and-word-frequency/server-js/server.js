/*
 Obtain a bearer token
 HTTP POST request.
 Include an Authorization header with the value of Basic <base64 encoded value from step 1>.
 Include a Content-Type header with the value of application/x-www-form-urlencoded;charset=UTF-8.
 The body of the request must be grant_type=client_credentials.
 */

var express = require('express');
var http = require('http');
var https = require('https');
var querystring = require('querystring');
var natural = require('natural');
//const crypto = require('crypto');
var stopwords = require('nltk-stopwords');
// load english stopwords
var englishStopwords = stopwords.load('english')
var stemmer = natural.PorterStemmer;

var app = express();
const port = 3000;


app.use('/', express.static(__dirname + '/../public'));

app.get('/', function (appReq, appRes) {
    appRes.sendFile('index.html');
});

/*
 * Getting consumer key to pass it as a RESTFUL Api parameter
 * for getting the secret bearer token in response
 */
app.get('/auth/:consumer', function(appReq, appRes){
    var token = appReq.params.consumer;

    var options = {
        hostname: 'api.twitter.com',
        path: '/oauth2/token',
        method: 'POST'
    };

    options.headers = {
        'Authorization' : 'Basic ' + token,
        'Content-Type'   :  'application/x-www-form-urlencoded;charset=UTF-8'
    };

    /*Requesting bearer token which would be received in response*/
    var req = https.request(options, (res) => {

            var body = [];
    res.on('data', (chunk) => {
        body.push(chunk);
});

    res.on('end', function() {

        var bodyString = body.join('');
        rsp = JSON.parse(bodyString);
        appRes.send(rsp);
        appRes.end();
    });

});

    req.write(querystring.stringify({
        "grant_type": 'client_credentials'
    }));
    req.end();

    req.on('error', (e) => {
        console.error(e);
});
});

/* search for Twitter feeds
 *  https://api.twitter.com/1.1/search/tweets.json?q=%23twitterapi&result_type=recent&lang=en
 *  %23 replaces the # hashtag
 *  GET method
 *  Authorization: Bearer AAA-token
 */
app.get('/search/:feed/:token', function(appReq, appRes){
    var token = appReq.params.token;
    var feed = appReq.params.feed;
    feed = encodeURIComponent(feed);

    //console.log('feed=' + feed);

    var options = {
        hostname: 'api.twitter.com',
        path: '/1.1/search/tweets.json?q=' + feed +'&lang=en',
        method: 'GET'
    };

    options.headers = {
        'Authorization' : 'Bearer ' + encodeURIComponent(token),
        'Content-Type'   :  'application/x-www-form-urlencoded;charset=UTF-8'
    };


    var req = https.request(options, (res) => {

            var body = [];
    res.on('data', (chunk) => {
        body.push(chunk);
});

    res.on('end', function() {

        var bodyString = body.join('');

        rsp = JSON.parse(bodyString);
        //console.log(rsp.statuses[0].text);
        appRes.send(rsp);
        appRes.end();
    });

});

    req.end();

    req.on('error', (e) => {
        console.error(e);
});
});

/* Using "Natural" Tokenizer to clean each word
 *  to make it analysis-ready
 */
app.get('/tokenizer/:words', function(appReq, appRes){

    var words = appReq.params.words;
    words = decodeURIComponent(words);

    var  tokenizer = new natural.WordTokenizer();
    words = stopwords.remove(words, englishStopwords);
    wordsList = words.split(' ');
    // stemming?
    console.log(wordsList[1]);
    var sentement = analyzeSentiment(wordsList);
    appRes.send(sentement);
    appRes.end();

    //testPy();
});

/*Using Sentimental node module to perform the sentiment analysis*/
function analyzeSentiment(words) {

    var result = {positive: 0, negative: 0, neutral: 0};
    var analyze = require('Sentimental').analyze;
    var score = 0;

    var w = '';

    for (var i=0; i<words.length; i++) {
        w = natural.PorterStemmer.stem(words[i]);
        score  = analyze(w).score;

        if (score < 0) {
            result.negative += 1;
        } else if (score > 0) {
            result.positive += 1;
        } else {
            result.neutral += 1;
        }
    }
    console.log(result);
    return result;
}


/*Individual word count for word frequency analysis*/
function addOrIncrementCount(term, totalResults) {
    var found = false;
    var term = term.trim();

    if (totalResults.length === 0) { // first word - just add
        var word = {'wordValue': term, 'count': 1};
        totalResults.push(word);

    } else {
        for (var j=0; j<totalResults.length; j++ ) {
            if (totalResults[j].wordValue === term || totalResults[j].wordValue.includes(term) ||  term.includes(totalResults[j].wordValue)) {
                found = true;
                totalResults[j].count += 1;
                break;
            }
        }

        if (!found) {
            var word = {'wordValue': term, 'count': 1};
            totalResults.push(word);
        }
    }

    return totalResults;
}
/*function testPy() {

 //start.js
 var spawn = require('child_process').spawn,
 py    = spawn('python', ['compute_input.py']),
 data = [1,2,3,4,5,6,7,8,9],
 dataString = '';

 py.stdout.on('data', function(data){
 dataString += data.toString();
 });
 py.stdout.on('end', function(){
 console.log('Sum of numbers=',dataString);
 });
 py.stdin.write(JSON.stringify(data));
 py.stdin.end();
 }*/

app.listen(port, function() {
    console.log('Express app listening on port ' + port);
});

