var BASE64_CONSUMER_STRING = 'WHpndXpjTE5HNE9tU09NeENlRmI2cEtuYjozeWNRWVp6UjliTEtqWTFFdkpaTUYxdXJPVll1N0xkTGVGUVMyM29vNEVTcjNnMUZYZg==';
var BEARER_TOKEN = '';

$(document).ready(function () {
	getBearerToken();
});

function getBearerToken() {
	
	$.ajax({
		  url: '/auth/' + BASE64_CONSUMER_STRING, 
		  success: function(data){
			if (data!== null) {
				if (data.access_token !== null) {
					BEARER_TOKEN = data.access_token;
					//console.log(BEARER_TOKEN);
				}
			}			
		  }
	});
}

function searchFeeds() {
	// validate user input
	// TODO prepare the string in case of multiple feeds separated by commas
	var divValidation = document.getElementById("validation");
	var userInput = document.getElementById("searchQry").value;
	
	if ( userInput === null || userInput === '') {
		divValidation.innerHTML = '<p style="color:red;">Search query can not be empty. </p>';
		return;
	} else {
		divValidation.innerHTML = '';
	}
	
	// query the Twitter Search API
	var finalURL = '/search/' + encodeURIComponent(userInput) +'/' + BEARER_TOKEN;
	var tmp = '';
	
	$.ajax({
		  url: finalURL, 
		  success: function(data){
			if (data!== null) {
				
				for (var i=0; i<data.statuses.length; i++) {
					tmp += data.statuses[i].text;
				}
				document.getElementById("searchResults").innerHTML = '<p>' +  tmp + ' </p>';
			}
		  },
		  complete: function() {
		    tokenize(tmp, userInput);
		  } 
	});
}