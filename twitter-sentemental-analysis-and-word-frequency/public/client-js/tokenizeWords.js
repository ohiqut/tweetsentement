	function inputKey(event) {
		event.which = event.which || event.keyCode;
		if(event.which === 13) {
			searchFeeds();
		}
	}


	function tokenize(str, userInput) {
	
		str = str.toLowerCase();
		userInput = userInput.toLowerCase();
		str = removeChar(str, userInput);
		str = str.replace(/[^a-zA-Z]/g,' ');
		
		str = encodeURIComponent(str);

		// tokenize & remove stop words
		$.ajax({
		  url: '/tokenizer/' + encodeURIComponent(str), 
		  success: function(data){
			
			/*var temp = '';
			// display tokenized words
			var divTokenizedWords = document.getElementById("tokenizedWords");
			if (data !== null && data.length !== 0) {
				for (var i=0; i<data.length; i++) {
					temp += data[i];
				}
				divTokenizedWords.innerHTML = '<p>' + temp + '</p>';*/
			if (data != 0) {				
				
				if (data.neutral == 0 && data.positive == 0 && data.negative ==0) {
				    clearControls();
				} else {
				    displayD3Chart(data);
				}

			} else {
				clearControls();
			}
		  					
			}						
		  }
		);
	
	}

	function clearControls() {
	
	document.getElementById("searchResults").innerHTML = '<p>No twitter feeds found to match your search criteria. </p>';
	document.getElementById("chart").innerHTML = '';
	document.getElementById("wordMap").innerHTML = '';
}

	function removeChar(str, ch) {

		while (str.includes(ch)) {
			str = str.replace(ch, ' ');
			str = str.replace('  ', ' ');
		}
		return str;
	}