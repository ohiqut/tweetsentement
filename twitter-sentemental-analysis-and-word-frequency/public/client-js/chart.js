	function inputKey(event) {
		event.which = event.which || event.keyCode;
		if(event.which === 13) {
			searchFeeds();
		}
	}


	function displayD3Chart(result) {
	
		// display stats
		if (result != null) {				
			document.getElementById("chart").innerHTML = '';
			
			// D3 chart
			var w = 300;
			var h = 350;

			var dataset = [ 
				{ key: 'negative', value: result.negative },
				{ key: 'positive', value: result.positive },
				{ key: 'neutral', value: result.neutral }];

			var xScale = d3.scale.ordinal()
							.domain(d3.range(dataset.length))
							.rangeRoundBands([0, w], 0.05); 

			var yScale = d3.scale.linear()
							.domain([0, d3.max(dataset, function(d) {return d.value;})])
							.range([20, h]);

			var key = function(d) {
				return d.key;
			};

			//Create SVG element
			var svg = d3.select("#chart")
						.append("svg")
						.attr("width", w)
						.attr("height", h);

			//Create bars
			svg.selectAll("rect")
			   .data(dataset, key)
			   .enter()
			   .append("rect")
			   .attr("x", function(d, i) {
					return xScale(i);
			   })
			   .attr("y", function(d) {
					return h - yScale(d.value);
			   })
			   .attr("width", xScale.rangeBand())
			   .attr("height", function(d) {
				   return yScale(d.value);
			   })
			   .attr("fill", function(d) {
				   var color = '';
					switch(d.key) {
						case 'negative':
							color = "rgb(0, 0, 0)";
							break;
						case 'positive':
							color = "rgb(0, 153, 51)";
							break;
						default:
							//neutral
							color = "rgb(153, 153, 153)";
					}
					return color;
			   })

				//Tooltip
				.on("mouseover", function(d) {
					//Get this bar's x/y values, then augment for the tooltip
					var xPosition = parseFloat(d3.select(this).attr("x")) + xScale.rangeBand() / 2;
					var yPosition = parseFloat(d3.select(this).attr("y")) + 14;
					
					//Update Tooltip Position & value
					d3.select("#tooltip")
						.style("left", xPosition + "px")
						.style("top", yPosition + "px")
						.select("#value")
						.text(d.value);
					d3.select("#tooltip").classed("hidden", false)
				})
				.on("mouseout", function() {
					//Remove the tooltip
					d3.select("#tooltip").classed("hidden", true);
				})	;

				//Create labels
				svg.selectAll("text")
				   .data(dataset, key)
				   .enter()
				   .append("text")
				   .text(function(d) {
						return d.key + ' ' + d.value;
				   })
				   .attr("text-anchor", "middle")
				   .attr("x", function(d, i) {
						return xScale(i) + xScale.rangeBand() / 2;
				   })
				   .attr("y", function(d) {
						return h - yScale(d.value) + 14;
				   })
				   .attr("font-family", "sans-serif") 
				   .attr("font-size", "12px")
				   .attr("fill", "white");
			// end D3 chart code
		}
	
	}

	function removeChar(str, ch) {

		while (str.includes(ch)) {
			str = str.replace(ch, ' ');
			str = str.replace('  ', ' ');
		}
		return str;
	}
	
	function displayTermFrequencyMap(data) {
	
	    var highCount = 100;

	/*
	html += "Most Common words"
    for w in words:
        size = str(int(15 + fdist[w] / float(highCount) * 150))
        colour = str(hex(int(0.8 * fdist[w] / float(highCount) * 256**3)))
        colour = colour[-(len(colour) - 2):]
        while len(colour) < 6:
            colour = "0" + colour
 
        html += "<span style='font-size: " + size + "px; color: #" + colour + "'>" + w + "</span> "
	*/
	    if (data != null ) {
	        var html = '', size, colour = '';
	        for (var i=0; i<data.length; i++ ){
	            console.log('word=' + data[i].wordValue + ' count=' + data[i].count);
	            size = 15 + data[i].count / highCount * 100;
	            colour = getRandomColour();
	            
	            html += "<span style='font-size: " + size + "px; color: " + colour + "'>" + data[i].wordValue + "</span> ";
	        }
	        
	        document.getElementById("wordMap").innerHTML = html;
	    }
	    
	}
	
	function getRandomColour() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
